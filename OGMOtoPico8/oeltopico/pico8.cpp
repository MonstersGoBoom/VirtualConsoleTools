#include "tool.h"

PICO8::PICO8(std::string filename)
{
	printf("Processing %s\n", filename.c_str());
	tfile = new Tokenized(filename, "\n\r");
}

void PICO8::ReplaceMap(std::string filename, MAP *tilemap)
{
	FILE *fp;
	printf("Writing %s\n", filename.c_str());

	fp = fopen(filename.c_str(), "w");
	bool write = true;
	if (fp != NULL)
	{
		write = true;
		while (tfile->hasTok())
		{
			std::string pop = tfile->sTok();

			if (tilemap->height > 32)
			{
				if (pop == "__gfx__")
				{
					fprintf(fp, "__gfx_\n");
					//	read and dump out the first 64 lines
					for (int q = 0; q < 64; q++)
					{
						pop = tfile->sTok();
						fprintf(fp, "%s\n", pop.c_str());
					}
					for (int y = 0; y < 32; y++)
					{
						for (int x = 0; x < 128; x++)
							fprintf(fp, "%02x", tilemap->cell(x, y));	 	//	lowercase hex only .. 

						fprintf(fp, "\n");
					}

					for (int q = 0; q < 64; q++)
					{
						pop = tfile->sTok(); // ignore this 
						for (int y = 32; y < 64; y++)
						{
							for (int x = 0; x < 64; x++)
								fprintf(fp, "%02x", tilemap->cell(x, y));	 	//	lowercase hex only .. 
							fprintf(fp, "\n");
							for (int x = 0; x < 64; x++)
								fprintf(fp, "%02x", tilemap->cell(64+x, y));	 	//	lowercase hex only .. 
							fprintf(fp, "\n");
						}
						fprintf(fp, "%s\n", pop.c_str());
					}

				}
			}
			//	replace the __map__ with the one from ogmo
			if (pop == "__map__")
			{
				int z = 0;
				fprintf(fp, "__map__\n");

				for (int y = 0; y < 32; y++)
				{
					for (int x = 0; x < 128; x++)
						fprintf(fp, "%02x", tilemap->cell(x, y));	 	//	lowercase hex only .. 

					fprintf(fp, "\n");
				}
				write = false;
			}

			if (pop == "__sfx__")
				write = true;

			if (write == true)
				fprintf(fp, "%s\n", pop.c_str());
		}
		fclose(fp);
	}
}

