#include "tool.h"
#include "ArgParse.h"

int main(int argc, const char** argv)
{
ArgumentParser parser;
PICO8 * pico = NULL;
TIC80 * tic80 = NULL;
MAP *tilemap = NULL;

	parser.addArgument("-t", "--tic80",1);
	parser.addArgument("-w", "--tic80world", 1);
	parser.addArgument("-p", "--pico8", 1);
	parser.addArgument("-i", "--input", 1);

	parser.addFinalArgument("output");

	printf(parser.usage().c_str());
	parser.parse(argc, argv);
	std::string _out = parser.retrieve<std::string>("output");
	std::string _tic80 = parser.retrieve<std::string>("tic80");
	std::string _tic80w = parser.retrieve<std::string>("tic80world");
	std::string _pico8 = parser.retrieve<std::string>("pico8");
	std::string _map = parser.retrieve<std::string>("input");

	if (_map != "")
	{
		tilemap = new MAP();
		if (_map.find(".oel") != std::string::npos)
			tilemap->processOGMO(_map);
		if (_map.find(".txm") != std::string::npos)
			tilemap->processTXM(_map);
		if (_map.find(".px") != std::string::npos)
			tilemap->processPX(_map);
		if (_map.find(".tmx") != std::string::npos)
				tilemap->processTMX(_map);
	}
	
	if (tilemap == NULL)
	{
		printf("error with map\n");
		exit(-1);
	}
	
	if (_tic80 != "")
	{
		tic80 = new TIC80(_tic80);
		tic80->ReplaceMap(_out, tilemap);
	}

	if (_tic80w != "")
	{
		tilemap->Write(_out,240, 136,true);
		//	write raw map
	//	tic80->ReplaceMap(_out, tilemap, true);
	}

	if (_pico8 != "")
	{
		pico = new PICO8(_pico8);
		pico->ReplaceMap(_out, tilemap);
	}

/*
	for (int x = 1; x < argc; x++)
	{
		std::string arg;
		arg = std::string(argv[x]);

		if (arg.find(".tic") != std::string::npos)
		{
			if (tic80 == NULL)
			{
				tic80 = new TIC80(arg);
			}
			else
			{
				if (tilemap == NULL)
				{
					printf("no tilemap file was specified, but two tic's ?\n");
					printf("try input.tic tilemap output.tic\n");
					exit(-1);
				}
				tic80->ReplaceMap(argv[x], tilemap,true);
			}
		}

		if (arg.find(".p8") != std::string::npos)
		{
			if (pico == NULL)
				pico = new PICO8(arg);
			else
			{
				//	found the second p8 
				if (tilemap == NULL)
				{
					printf("no tilemap file was specified, but two p8's ?\n");
					printf("try input.p8 tilemap output.p8\n");
					exit(-1);
				}
				pico->ReplaceMap(argv[x], tilemap);
				MemTrack::TrackListMemoryUsage();
			}
		}
		if (arg.find(".oel") != std::string::npos)
		{
			if (tilemap == NULL)
			{
				tilemap = new MAP();
				tilemap->processOGMO(arg);
			}
		}
		if (arg.find(".txm") != std::string::npos)
		{
			if (tilemap == NULL)
			{
				tilemap = new MAP();
				tilemap->processTXM(arg);
			}
		}
		if (arg.find(".px") != std::string::npos)
		{
			if (tilemap == NULL)
			{
				tilemap = new MAP();
				tilemap->processPX(arg);
			}
		}

		if (arg.find(".tmx") != std::string::npos)
		{
			if (tilemap == NULL)
			{
				tilemap = new MAP();
				tilemap->processTMX(arg);
			}
		}
	}
*/
	if (tilemap != NULL) delete tilemap;
	if (pico != NULL) delete pico;
	if (tic80 != NULL) delete tic80;
//	MemTrack::TrackDumpBlocks();
//	MemTrack::TrackListMemoryUsage();
}


