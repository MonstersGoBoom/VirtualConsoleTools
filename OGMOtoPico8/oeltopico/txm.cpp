#include "tool.h"

bool MAP::processTXM(std::string filename)
{
//	Tokenized *file = new Tokenized(std::string(filename), " <>\"\',\n\r=");
	printf("Processing %s\n", filename.c_str());
	Tokenized *file = new Tokenized(filename, "\n\r");
	height = (int)file->strings.size();
	width = 0;
	int lineindex = 0;
	while (file->hasTok())
	{
		std::string pop = file->sTok();
		Tokenized *line = new Tokenized(pop,", ",false);
		if (width == 0)
		{
			width = (int)line->strings.size();
			buffer = new unsigned char[width * height];
		}
		for (int x = 0; x < width; x++)
			buffer[(lineindex * width) + x] = line->iTok();
		lineindex++;
		delete line;
	}
	delete file;
	/*
	while (file->hasTok())
	{
		std::string pop = file->sTok();
		if (pop == "width")
			width = file->iTok();
		if (pop == "height")
			height = file->iTok();
		if (pop == "csv")
		{
			buffer = new unsigned char[width * height];
			for (int x = 0; x < width * height; x++)
			{
				int v = file->iTok();
				buffer[x] = (unsigned char)(v - 1);
			}
		}
	}
	printf("read %d x %d map\n", width, height);
*/
	return true;
}
