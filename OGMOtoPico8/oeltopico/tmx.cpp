#include "tool.h"


bool MAP::processTMX(std::string filename)
{
	printf("Processing %s\n", filename.c_str());

	Tokenized *file = new Tokenized(filename, " <>\"\',\n\r=");

	while (file->hasTok())
	{
		std::string pop = file->sTok();
		if (pop == "width")
			width = file->iTok();
		if (pop == "height")
			height = file->iTok();
		if (pop == "csv")
		{
			buffer = new unsigned char[width * height];
			for (int x = 0; x < width * height; x++)
			{
				int v = file->iTok();
				buffer[x] = (unsigned char)(v-1);
			}
		}
	}
	delete file;
	printf("read %d x %d map\n", width, height);
	return true;
}
