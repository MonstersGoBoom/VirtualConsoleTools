#include "tool.h"

bool MAP::processOGMO(std::string filename)
{
	Tokenized *file = new Tokenized(filename, " <>\"\',\n\r=");

	while (file->hasTok())
	{
		std::string pop = file->sTok();
		if (pop == "width")
			width = file->iTok();
		if (pop == "height")
			height = file->iTok();
		if (pop == "CSV")
		{
			width /= 8;
			height /= 8;
			buffer = new unsigned char[width * height];
			for (int x = 0; x < width * height; x++)
			{
				int v = file->iTok();
				if (v == -1) v = 0;	//	if it's -1 that's unused so set it to zero 
				buffer[x] = (unsigned char)v;
			}
		}
	}
	printf("read %d x %d map\n", width, height);
	return true;
}

//	safely return the cell from x & y 

unsigned char MAP::cell(int x, int y)
{
	if (buffer == NULL)
		return 0;
	if ((x < 0) || (x >= width) || (y < 0) || (y >= height))
		return 0;
	return (buffer[x + (y*width)]);
}
