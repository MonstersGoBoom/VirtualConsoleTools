#pragma once
typedef struct
{
	uint32_t type : 8;
	uint32_t size : 24;
}TIC_disk_chunk;

typedef enum
{
	None,
	Tiles,
	Sprites,
	Cover,
	Map,
	Code,
	Sound,
	Music,
	Max
} TIC80_Type;

class TIC80_chunk
{
public:
	TIC_disk_chunk header;
	unsigned char *buffer;
};

class TIC80
{
public:
	TIC80(std::string filename);
	~TIC80();
	void ReplaceMap(std::string filename, MAP *ogmo);
private:
	TIC80_chunk chunks[Max];
	unsigned char *buffer;
};
