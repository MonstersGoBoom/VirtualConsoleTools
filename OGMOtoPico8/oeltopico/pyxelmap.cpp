#include "tool.h"

bool MAP::processPX(std::string filename)
{
	printf("Processing %s\n", filename.c_str());
	Tokenized *file = new Tokenized(filename, " <>\"\',\n\r=");
	while (file->hasTok())
	{
		std::string pop = file->sTok();
		if (pop.find("tileswide") != std::string::npos)
			width = file->iTok();
		if (pop.find("tileshigh") != std::string::npos)
			height = file->iTok();
		if (pop.find("layer") != std::string::npos)
		{
			int lindex = file->iTok();
			buffer = new unsigned char[width * height];
			for (int x = 0; x < width * height; x++)
			{
				int v = file->iTok();
				buffer[x] = (unsigned char)(v);
			}
		}
	}
	delete file;
	return true;
}
